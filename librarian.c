#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "book.h"
#include "copy.h"
#include "librarian.h"
#include "user.h"
#include "issuerecord.h"
#include "payment.h"

void librarianMenu()
{
    int choice,tempid,memid;
    char name[20];
    do
    {
        printf("0.signout \n1.edit profile \n2.change password \n3.find book by name \n4.check book avilable \n5.add new book \n6.add new copy \n7.issue book copy \n8.return book copy \n9.list issued books \n10.edit book \n11.chnage rack \n12.add User \n13.take payment \n14.payment history");
        printf("\nEnter the choice : ");
        scanf("%d",&choice);
        switch (choice)
        {
        case 0:
            break;
        case 1:
            printf("Enter Email : ");
            scanf("%s",&name);
            editProfile(name);
            break;
        case 2:
            printf("Enter Email Id : ");
            scanf("%s",&name);
            changePassword(name);
            break;
        case 3:
            printf("\nEnter the Name : ");
            scanf("%s",name);
            getBookByName(name);
            break;
        case 4:
            printf("\nEnter the BookId : ");
            scanf("%d",&tempid);
            printf("Book Copies Available : %d\n",getCopyAvilable(tempid));
            break;
        case 5:
            addBook();
            break;                    
        case 6:
            printf("\nEnter the BookId : ");
            scanf("%d",&tempid);
            addCopy(tempid);
            printf("Book Copy Added \n");
            break;        
        case 7:
            addIssue();
            break;
        case 8:
            printf("Enter BookID : ");
            scanf("%d",&tempid);
            printf("Enter MemberID : ");
            scanf("%d",&memid);
            returnBook(tempid,memid);
            break;                        
        case 9:
            showRecord();
            break;            
        case 10:
            printf("\nEnter the BookId : ");
            scanf("%d",&tempid);
            editBook(tempid);
            break;
        case 11:
            printf("\nEnter the BookId : ");
            scanf("%d",&tempid);
            changeRack(tempid);
            break;                        
        case 12:
            signUp();
            break;
        case 13:
            takePayment();
            break;
        case 14:
            paymentHistory();
            break;            
        default:
            break;
        }

    } while (choice!=0);
    
    
}