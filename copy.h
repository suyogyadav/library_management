#ifndef __COPY_H
#define __COPY_H

struct Copy
{
    int id , bookid , rack ,status;
};

int addCopy(int bookid);
int getCopyRack(int bookid);
int getCopyAvilable(int bookid);
void changeRack(int id);
void addBackCopy(int bookid);
int deductCopy(int bookid);
#endif