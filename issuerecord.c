#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "issuerecord.h"
#include "count.h"
#include "payment.h"
#include "copy.h"

struct IssueRecord record;
date_t tempdate;

void addIssue()
{
    FILE *fp;
    int IRcount = getIssueCount() ;

    record.id = IRcount;
    printf("Enter BookId : ");
    scanf("%d",&record.bookid);
    printf("Enter MemberId : ");
    scanf("%d",&record.memberid);
    
    if (paidMember(record.memberid))
    {
        if (deductCopy(record.bookid))
        {
            date_accept(&tempdate);
            // tempdate = date_current();
            record.issuedate = tempdate;
            record.deudate = date_add(tempdate,15);
            record.fineamt = 0;

            fp = fopen("record.db","ab");
            if(fp == NULL) {
                perror("cannot open record file");
                exit(1);
            }
            fwrite(&record,sizeof(record),1,fp);
            fclose(fp);
                
            incIssueCount();
            printf("Book Issued Sucessfully \n");
        }
        else
        {
            printf("\nNo Copies Of Book Available \n");
        }
    }
    else
    {
        printf("Not A Paid Member \n");
    }
}

void returnBook(int id,int memid)
{
    int found=0;
    FILE *fp;
    fp = fopen("record.db","rb+");
    if(fp == NULL) {
		perror("cannot open record file");
		exit(1);
	}
    while (fread(&record,sizeof(record),1,fp)>0)
    {
        if (record.bookid == id && record.memberid == memid)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        int days = date_cmp(date_current(),record.issuedate);
        printf("Delay in Return : %d days \n",days);
        if (days != 0)
        {
            record.fineamt = (days*5);
            printf("Please Pay Fine First : %d rs \n",record.fineamt);
            takePayment();
        }
        else
        {
            record.fineamt = 0;
        }
        record.returndate = date_current();
        addBackCopy(record.bookid);
        fseek(fp,-(sizeof(record)),SEEK_CUR);
        fwrite(&record,sizeof(record),1,fp);
        printf("Book Returned \n");

    }else
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
}

void showRecord()
{
    int found=0;
    FILE *fp;
    fp = fopen("record.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&record,sizeof(record),1,fp)>0)
    {
        printf("IssueId : %d \n",record.id);
        printf("BookId : %d \n",record.bookid);
        printf("MemberId : %d \n",record.memberid);
        printf("Issue ");
        date_print(&record.issuedate);
        printf("Deu ");
        date_print(&record.deudate);
        printf("Return ");
        date_print(&record.returndate);
        printf("Fine : %d \n",record.fineamt);
    }

    fclose(fp);
}