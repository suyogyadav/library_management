#ifndef __BOOK_H
#define __BOOK_H

struct Book
{
    int id , price , isbn;
    char name[10];
    char author[50];
    char subject[20];
};

void addBook();
void getBookById(int id);
void getBookByName(char name[]);
void getBookBySubject(char subject[]);
void getBookBySubject(char author[]);
void printBookDetails(int id);
void editBook(int id);

#endif