#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "book.h"
#include "owner.h"
#include "user.h"
#include "copy.h"
#include "payment.h"

void ownerMenu()
{
    int ch,uid;
    char sub[20];
    do
    {
        printf("0.signOut \n1.editProfile \n2.change password \n3.subject wise copy \n4.bookwise copy \n5.fee or fine report \n6.Apoint Librarian \nEnter Your Choice : ");
        scanf("%d",&ch);
        switch (ch)
        {
            case 0:
                break;

            case 1:
                printf("Enter Email Id : ");
                scanf("%s",sub);
                editProfile(sub);
                break;

            case 2:
                printf("Enter Email Id : ");
                scanf("%s",sub);
                changePassword(sub);
                break;

            case 3:
                printf("Enter subject Name : ");
                scanf("%s",sub);
                getBookBySubject(sub);
                break;

            case 4:
                printf("Enter book Id : ");
                scanf("%d",&uid);
                printf("Book Copies Available : %d\n",getCopyAvilable(uid));
                break;
            
            case 5:
                paymentHistory();
                break;
            
            case 6:
                signUp();
                break;

            default:
                break;
        }
    } while (ch!=0);
}