#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "user.h"
#include "owner.h"
#include "librarian.h"
#include "member.h"
#include "user.h"
#include "count.h"

struct User user;

void addOwner()
{
    int found=0;
    FILE *fp;
    fp = fopen("user.db","rb+");
    if(fp == NULL) {
        
        user.id = 0;
        strcpy(user.name,"Owner");
        strcpy(user.email,"OwnerEmail");
        user.phone = 1234;
        strcpy(user.pass,"1234");
        strcpy(user.role,"Owner");

        fp = fopen("user.db","ab");
        fwrite(&user,sizeof(user),1,fp);
        fclose(fp);
	}
    else
    {
        while (fread(&user,sizeof(user),1,fp)>0)
        {
            if (user.id == 0)
            {
                found=1;
                break;
            }
        }
        if (found==1)
        {
            return;
        }else
        {
            fp = fopen("user.db","rb+");
            
            user.id = 0;
            strcpy(user.name,"Owner");
            strcpy(user.email,"OwnerEmail");
            user.phone = 1234;
            strcpy(user.pass,"1234");
            strcpy(user.role,"Owner");

            fp = fopen("user.db","ab");
            fwrite(&user,sizeof(user),1,fp);
            fclose(fp);

            return;
        }   
    }
}

void editProfile(char email[])
{
    int found=0;
    FILE *fp;
    fp = fopen("user.db","rb+");
    if(fp == NULL) {
		perror("cannot open user file");
		exit(1);
	}
    while (fread(&user,sizeof(user),1,fp)>0)
    {
        if (strcmp(user.email,email) == 0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        printf("Enter Name : ");
        scanf("%s",&user.name);
        printf("Enter Email : ");
        scanf("%s",&user.email);
        printf("Enter Phone : ");
        scanf("%d",&user.phone);

        fseek(fp,-(sizeof(user)),SEEK_CUR);
        fwrite(&user,sizeof(user),1,fp);
        printf("Profile Updated \n");

    }else
    {
        printf("User Not Found");
    }
    fclose(fp);
}


void changePassword(char email[])
{
    int found=0;
    FILE *fp;
    fp = fopen("user.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&user,sizeof(user),1,fp)>0)
    {
        if (strcmp(user.email,email) == 0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        printf("Enter new Email : ");
        scanf("%s",&user.email);
        printf("Enter new pass : ");
        scanf("%s",&user.pass);
        fseek(fp,-(sizeof(user)),SEEK_CUR);
        fwrite(&user,sizeof(user),1,fp);
        printf("Email & Password Updated \n");
    }else
    {
        printf("User Not Found");
    }
    fclose(fp);
}

void signIn()
{
    int found = 0;
    char email[50];
    char pass[10];
    FILE *fp;

    printf("Enter Email : ");
    scanf("%s",&email);
    printf("Enter Password : ");
    scanf("%s",&pass);
    fp = fopen("user.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}

    while (fread(&user,sizeof(user),1,fp)>0)
    {
         if (strcmp(email,user.email) == 0)
        {
            if (strcmp(pass,user.pass) == 0)
            {
                printf("Signin Succesfull\n"); 
                if (strcmp(user.role,"Owner") == 0)
                {
                    ownerMenu();
                }
                if (strcmp(user.role,"Librarian") == 0)
                {
                    fclose(fp);
                    librarianMenu();
                }
                if (strcmp(user.role,"Member") == 0)
                {
                    fclose(fp);
                    memberMenu();
                }
            }
            
        }
    }

}

void signUp()
{
    int count = getUserCount();
    FILE *fp;
    int ch;
    user.id = count;
    printf("Enter Name : ");
    scanf("%s",&user.name);
    printf("Enter Email : ");
    scanf("%s",&user.email);
    printf("Enter Phone : ");
    scanf("%d",&user.phone);
    printf("Enter Pass : ");
    scanf("%s",&user.pass);
    printf("Select Role : \n1.Librarian \n2.Member \n");
    scanf("%d",&ch);

    switch (ch)
    {
    case 1:
        strcpy (user.role , "Librarian");
        break;
    
    case 2:
        strcpy (user.role , "Member");
        break;

    default:
        strcpy (user.role , "Member");
        break;
    }
    fp = fopen("user.db","ab");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    fwrite(&user,sizeof(user),1,fp);
    printf("New User Added \n");
    fclose(fp);
    incUserCount;
}