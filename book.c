#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "book.h"
#include "copy.h"
#include "count.h"

struct Book book;

void addBook()
{
    int Bcount = getBookCount();
    FILE *fp;
    book.id = Bcount;
    printf("Enter Book Name : ");
    scanf("%s",book.name);
    printf("Enter author Name : ");
    scanf("%s",book.author);
    printf("Enter subject Name : ");
    scanf("%s",book.subject);
    printf("Enter price : ");
    scanf("%d",&book.price);
    printf("Enter isbn : ");
    scanf("%d",&book.isbn);

    fp = fopen("book.db","ab");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    fwrite(&book,sizeof(book),1,fp);
    fclose(fp);

    addCopy(Bcount);
    printBookDetails(Bcount);
    printf("New Book Added \n");
    incBookCount();
}

void getBookById(int id)
{
   printBookDetails(id);
}

void getBookByName(char name[])
{
    int found=0;
    FILE *fp;
    fp = fopen("book.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&book,sizeof(book),1,fp)>0)
    {
        if (strcmp(name,book.name)==0)
        {
            found = 1;
            break;
        }
    }
    if (found==1)
    {
        printBookDetails(book.id);
    }else
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
}

void getBookBySubject(char subject[])
{
    int found=0;
    FILE *fp;
    fp = fopen("book.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&book,sizeof(book),1,fp)>0)
    {
        if (strcmp(subject,book.subject)==0)
        {
            found = 1;
            printBookDetails(book.id);
        }
    }
    if (found==0)
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
}

void getBookByAuthor(char author[])
{
    int found=0;
    FILE *fp;
    fp = fopen("book.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&book,sizeof(book),1,fp)>0)
    {
        if (strcmp(author,book.author)==0)
        {
            found = 1;
            break;
        }
    }
    if (found==1)
    {
        printBookDetails(book.id);
    }else
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
}

void printBookDetails(int id)
{
    int found=0;
    FILE *fp;
    fp = fopen("book.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&book,sizeof(book),1,fp)>0)
    {
        if (book.id == id)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        printf("Book id = %d \n",book.id);
        printf("Book name = %s \n",book.name);
        printf("Authhor name = %s \n",book.author); 
        printf("Subject name = %s \n",book.subject);
        printf("price = %d \n",book.price);
        printf("isbn = %d \n",book.isbn);
    }else
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
}

void editBook(int id)
{
    int found=0;
    FILE *fp;
    fp = fopen("book.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&book,sizeof(book),1,fp)>0)
    {
        if (book.id == id)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        book.id = id;
        printf("Enter Book Name : ");
        scanf("%s",&book.name);
        printf("Enter author Name : ");
        scanf("%s",&book.author);
        printf("Enter subject Name : ");
        scanf("%s",&book.subject);
        printf("Enter price : ");
        scanf("%d",&book.price);
        printf("Enter isbn : ");
        scanf("%d",&book.isbn);

        fseek(fp,-(sizeof(book)),SEEK_CUR);
        fwrite(&book,sizeof(book),1,fp);
        printf("Book Updated \n");

    }else
    {
        printf("Book Not Found \n");
    }
    fclose(fp);
    printBookDetails(id);
}