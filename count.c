#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"count.h"

struct Count count;

int getBookCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {

        fp = fopen("count.db","ab");
        fwrite(&count,sizeof(count),1,fp);
        fclose(fp);
        
	}
    else
    {
        while (fread(&count,sizeof(count),1,fp)>0)
        {
            if (count.bookCount > 0)
            {
                found=1;
                break;
            }
        }
        
        if (found==1)
        {
            fclose(fp);
            return count.bookCount;
        }else
        {
            fclose(fp);
            return 0;
        }   
    }
}

void incBookCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {
		perror("cannot open count file");
		exit(1);
	}
    while (fread(&count,sizeof(count),1,fp)>0)
    {
        if (count.bookCount>=0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        count.bookCount = count.bookCount + 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }else
    {
        count.bookCount = 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }
    fclose(fp);
}

int getCopyCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {

        fp = fopen("count.db","ab");
        fwrite(&count,sizeof(count),1,fp);
        fclose(fp);
        
	}
    else
    {
        while (fread(&count,sizeof(count),1,fp)>0)
        {
            if (count.copyCount > 0)
            {
                found=1;
                break;
            }
        }
        
        if (found==1)
        {
            fclose(fp);
            return count.copyCount;
        }else
        {
            fclose(fp);
            return 0;
        }   
    }
}

void incCopyCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {
		perror("cannot open count file");
		exit(1);
	}
    while (fread(&count,sizeof(count),1,fp)>0)
    {
        if (count.copyCount>=0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        count.copyCount = count.copyCount + 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }else
    {
        count.copyCount = 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }
    fclose(fp);
}

int getIssueCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {

        fp = fopen("count.db","ab");
        fwrite(&count,sizeof(count),1,fp);
        fclose(fp);
        
	}
    else
    {
        while (fread(&count,sizeof(count),1,fp)>0)
        {
            if (count.issueCount > 0)
            {
                found=1;
                break;
            }
        }
        
        if (found==1)
        {
            fclose(fp);
            return count.issueCount;
        }else
        {
            fclose(fp);
            return 0;
        }   
    }
}

void incIssueCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {
		perror("cannot open count file");
		exit(1);
	}
    while (fread(&count,sizeof(count),1,fp)>0)
    {
        if (count.issueCount>=0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        count.issueCount = count.issueCount + 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }else
    {
        count.issueCount = 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }
    fclose(fp);
}

int getUserCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {

        fp = fopen("count.db","ab");
        fwrite(&count,sizeof(count),1,fp);
        fclose(fp);
        
	}
    else
    {
        while (fread(&count,sizeof(count),1,fp)>0)
        {
            if (count.userCount > 0)
            {
                found=1;
                break;
            }
        }
        
        if (found==1)
        {
            fclose(fp);
            return count.userCount;
        }else
        {
            fclose(fp);
            return 0;
        }   
    }
}

void incUserCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {
		perror("cannot open count file");
		exit(1);
	}
    while (fread(&count,sizeof(count),1,fp)>0)
    {
        if (count.userCount>=0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        count.userCount = count.userCount + 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }else
    {
        count.userCount = 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }
    fclose(fp);
}

int getPaymentCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {

        fp = fopen("count.db","ab");
        fwrite(&count,sizeof(count),1,fp);
        fclose(fp);
        
	}
    else
    {
        while (fread(&count,sizeof(count),1,fp)>0)
        {
            if (count.paymentCount > 0)
            {
                found=1;
                break;
            }
        }
        
        if (found==1)
        {
            fclose(fp);
            return count.paymentCount;
        }else
        {
            fclose(fp);
            return 0;
        }   
    }
}

void incPaymentCount()
{
    int found=0;
    FILE *fp;
    fp = fopen("count.db","rb+");
    if(fp == NULL) {
		perror("cannot open count file");
		exit(1);
	}
    while (fread(&count,sizeof(count),1,fp)>0)
    {
        if (count.paymentCount>=0)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        count.paymentCount = count.paymentCount + 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }else
    {
        count.paymentCount = 1;
        fseek(fp,-(sizeof(count)),SEEK_CUR);
        fwrite(&count,sizeof(count),1,fp);
    }
    fclose(fp);
}