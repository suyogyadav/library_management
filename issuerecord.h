#ifndef __ISSUERECORD_H
#define __ISSUERECORD_H

#include"date.h"

struct IssueRecord
{
    int id , bookid, memberid ,fineamt;
    date_t issuedate, deudate, returndate;
    
};

void addIssue();
void returnBook(int id,int memid);
void showRecord();
#endif