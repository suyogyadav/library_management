#ifndef __PAYMENT_H
#define __PAYMENT_H

#include"date.h"

struct payment
{
    int id , userid , amt;
    char type[10];
    date_t trandate, nxtdeudate;
};

void takePayment();
void paymentHistory();
void mamberHistory(int id);
int paidMember(int id);
#endif