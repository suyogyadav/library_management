#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "payment.h"
#include "count.h"

struct payment payrecord;

void takePayment()
{
    FILE *fp;
    int paycount = getPaymentCount();

    payrecord.id = paycount;
    printf("Enter the User Id : ");
    scanf("%d",&payrecord.userid);
    printf("Enter the amount : ");
    scanf("%d",&payrecord.amt);
    printf("Enter the type : ");
    scanf("%s",payrecord.type);
    payrecord.trandate = date_current();
    payrecord.nxtdeudate = date_add(date_current(),30);

    fp = fopen("payment.db","ab");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    fwrite(&payrecord,sizeof(payrecord),1,fp);
    fclose(fp);
    incPaymentCount();
    printf("Payment Sucessfull \n");
}

void paymentHistory()
{
    int found=0;
    FILE *fp;
    fp = fopen("payment.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&payrecord,sizeof(payrecord),1,fp)>0)
    {
        printf("Payment Id : %d \n",payrecord.id);
        printf("User Id : %d \n",payrecord.userid);
        printf("Amount : %d \n",payrecord.amt);
        printf("Type : %s \n",payrecord.type);
        printf("Trans ");
        date_print(&payrecord.trandate);
        printf("Next Deu ");
        date_print(&payrecord.nxtdeudate);
    }

    fclose(fp);
}

void mamberHistory(int id)
{
    int found=0;
    FILE *fp;
    fp = fopen("payment.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&payrecord,sizeof(payrecord),1,fp)>0)
    {
        if (payrecord.userid == id)
        {
            printf("Payment Id : %d \n",payrecord.id);
            printf("User Id : %d \n",payrecord.userid);
            printf("Amount : %d \n",payrecord.amt);
            printf("Type : %s \n",payrecord.type);
            printf("Trans ");
            date_print(&payrecord.trandate);
            printf("Next Deu ");
            date_print(&payrecord.nxtdeudate);
        }
    }
    
    fclose(fp);
}

int paidMember(int id)
{
    int found=0,paid = 0;
    FILE *fp;
    fp = fopen("payment.db","rb+");
    if(fp == NULL) {
        //printf("Not A Paid Member");
        return 0;
	}
    else
    {
        while (fread(&payrecord,sizeof(payrecord),1,fp)>0)
        {
            if (payrecord.userid == id && payrecord.amt == 500 )
            {
                found = 1;
                paid = 1;
            }
        }
        fclose(fp);
        if (found)
        {
            return 1;
        }
        else
        {
            return 0;
        }           
    }
}