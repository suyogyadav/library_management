#ifndef __COUNT_H
#define __COUNT_H

struct Count
{
    int bookCount,copyCount,issueCount,userCount,paymentCount;
};

int getBookCount();
void incBookCount();
int getCopyCount();
void incCopyCount();
int getIssueCount();
void incIssueCount();
int getUserCount();
void incUserCount();
int getPaymentCount();
void incPaymentCount();

#endif