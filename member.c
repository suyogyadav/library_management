#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"user.h"
#include"member.h"
#include"issuerecord.h"
#include"payment.h"
#include"book.h"
#include"copy.h"

void memberMenu()
{
    int memberchoice,tempid;
    char name[10];
    do
    {
        printf("0.signout \n1.Edit Profile \n2.Change Password \n3.Find Book By Name \n4.Check Book Available \n5.List Issued Books \n6.Payment History \nEnter Choice : ");
        scanf("%d",&memberchoice);
        switch (memberchoice)
        {
            case 0:        
                break;

            case 1:        
                printf("Enter Email Id : ");
                scanf("%s",&name);
                editProfile(name);
                break;

            case 2:        
                printf("Enter Email Id : ");
                scanf("%s",&name);
                changePassword(name);
                break;

            case 3:
                printf("\nEnter the Name : ");
                scanf("%s",name);
                getBookByName(name);
                break;

            case 4:
                printf("\nEnter the BookId : ");
                scanf("%d",&tempid);
                printf("Book Copies Available : %d\n",getCopyAvilable(tempid));
                break;

            case 5:
                showRecord();
            break;

            case 6:
                printf("Enter user Id : ");
                scanf("%d",&tempid);
                mamberHistory(tempid);
            break;

            default:
                break;
        }
    } while (memberchoice!=0);
}
