#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "copy.h"
#include "count.h"

struct Copy copy;

int addCopy(int bookid)
{
    int copyCount = getCopyCount();
    FILE *fp;

    copy.bookid = bookid;
    printf("Enter rackNO : ");
    scanf("%d",&copy.rack);
    printf("Enter No of Copies : ");
    scanf("%d",&copy.status);

    fp = fopen("copy.db","ab");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    fwrite(&copy,sizeof(copy),1,fp);
    fclose(fp);

    incCopyCount();
}

int getCopyRack(int bookid)
{
    int found=0;
    FILE *fp;
    fp = fopen("copy.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&copy,sizeof(copy),1,fp)>0)
    {
        if (copy.id == bookid)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        fclose(fp);
        return copy.rack;
    }else
    {
        fclose(fp);
        return 0;
    }
}

int getCopyAvilable(int bookid)
{
    int found=0,add=0;
    FILE *fp;
    fp = fopen("copy.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&copy,sizeof(copy),1,fp)>0)
    {
        if (copy.bookid == bookid)
        {
            found = 1;
            add = add + copy.status;
        }
    }
    
    if (found==1)
    {
        fclose(fp);
        return add;
    }else
    {
        fclose(fp);
        return 0;
    }
}

void changeRack(int id)
{
    int found=0;
    FILE *fp;
    fp = fopen("copy.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&copy,sizeof(copy),1,fp)>0)
    {
        if (copy.id == id)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        copy.id = id;
        printf("Enter rackNO : ");
        scanf("%d",&copy.rack);
        printf("Enter status : ");
        scanf("%d",&copy.status);

        fseek(fp,-(sizeof(copy)),SEEK_CUR);
        fwrite(&copy,sizeof(copy),1,fp);
        printf("Copy Updated \n");

    }else
    {
        printf("Copy Not Found\n");
    }
    fclose(fp);
}

int deductCopy(int bookid)
{
    int found=0,add=0;
    FILE *fp;
    fp = fopen("copy.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&copy,sizeof(copy),1,fp)>0)
    {
        if (copy.bookid == bookid)
        {
            if (copy.status > 0)
            {
                found = 1;
                break;
            }
            
        }
    }
    
    if (found==1)
    {
        copy.status = copy.status-1;
        fseek(fp,-(sizeof(copy)),SEEK_CUR);
        fwrite(&copy,sizeof(copy),1,fp);
        fclose(fp);
        return 1;
    }else
    {
        fclose(fp);
        return 0;
    }
}

void addBackCopy(int bookid)
{
    int found=0,add=0;
    FILE *fp;
    fp = fopen("copy.db","rb+");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while (fread(&copy,sizeof(copy),1,fp)>0)
    {
        if (copy.bookid == bookid)
        {
            found = 1;
            break;
        }
    }
    
    if (found==1)
    {
        copy.status = copy.status + 1;
        fseek(fp,-(sizeof(copy)),SEEK_CUR);
        fwrite(&copy,sizeof(copy),1,fp);
    }
    
    fclose(fp);
}