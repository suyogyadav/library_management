#ifndef __USER_H
#define __USER_H

struct User
{
    int id;
    char name[10];
    char email[50];
    int phone;
    char pass[10];
    char role[10];
};

void editProfile(char email[]);
void changePassword(char email[]);
void addOwner();
void signIn();
void signUp();
int compare_string(char *first, char *second);
void createRandomUsers();
void showRandomUsers();
void ownerMenu();

#endif